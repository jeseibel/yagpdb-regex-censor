{{/* 
This script will listen for blocked words and if a blocked word is deteccted:
direct message the offender, delete the message, and send a warning message in the channel.
*/}}

{{/* require blocked text to either have a space before or be the first word */}}
{{ $PREFIX := "(?:^|[^a-zA-Z0-9])+" }}
{{/* require blocked text to either be the last word or have a space after it */}}
{{ $SUFIX := "(?:$|[^a-zA-Z0-9])+" }}

{{/* allow any number of non-letter characters between each letter */}}
{{ $SPLITTER := "[^a-zA-Z0-9]*" }}
{{/* also allow certain special characters */}}
{{ $SPLITTER_SPECIAL := "[^a-zA-Z0-9:\"~_*]*" }}

{{ $a := "[аАaA@ä]" }}
{{ $b := "[bB]" }}
{{ $c := "[cC]" }}
{{ $d := "[dD]" }}
{{ $e := "[eE3é]" }}
{{ $f := "[fF]" }}
{{ $g := "[gG]" }}
{{ $h := "[hH]" }}
{{ $i := "[1li|LI!]" }}
{{ $j := "[jJ]" }}
{{ $k := "[kK]" }}
{{ $l := $i }}
{{ $m := "[mM]" }}
{{ $n := "[nN]" }}
{{ $o := "[oO0]" }}
{{ $p := "[pP]" }}
{{ $q := "[qQ]" }}
{{ $r := "[rR]" }}
{{ $s := "[sSzZ$5]" }}
{{ $t := "[tT]" }}
{{ $u := "[uU]" }}
{{ $v := "[vV]" }}
{{ $w := "[wW]" }}
{{ $x := "[xX]" }}
{{ $y := "[yY]" }}
{{ $z := $s }}

{{/* ^\b$ will never match anything and is a comment for the following regex. Since I can't put comments in otherwise :/ */}}
{{ $acronymRegex := (joinStr "|"
"^\b$  shi"
(joinStr "" $PREFIX (joinStr $SPLITTER $s $h $i) $SUFIX)
"^\b$  ffs"
(joinStr "" $PREFIX (joinStr $SPLITTER $f $f $s) $SUFIX)
"^\b$  fu"
(joinStr "" $PREFIX (joinStr $SPLITTER $f $u) $SUFIX)
"^\b$  lmfao"
(joinStr "" $PREFIX (joinStr $SPLITTER $l $m $f $a (joinStr "" $o "*")) $SUFIX)
"^\b$  fml"
(joinStr "" "(?:^|[^A-z0-9]forge.)+" (joinStr $SPLITTER $f $m $l) $SUFIX)
"^\b$  nss"
(joinStr "" $PREFIX (joinStr $SPLITTER $n $s $s) $SUFIX)
"^\b$  idfk/c"
(joinStr "" $PREFIX (joinStr $SPLITTER $i $d $f "[kKcC]") $SUFIX)
"^\b$  stfu"
(joinStr "" $PREFIX (joinStr $SPLITTER $s $t $f $u) $SUFIX)
"^\b$  bamf"
(joinStr "" $PREFIX (joinStr $SPLITTER $b $a $m $f) $SUFIX)
"^\b$  gtfo"
(joinStr "" $PREFIX (joinStr $SPLITTER $g $t $f $o) $SUFIX)
"^\b$  omfg"
(joinStr "" $PREFIX (joinStr $SPLITTER $o $m $f $g) $SUFIX)
"^\b$  atfo"
(joinStr "" $PREFIX (joinStr $SPLITTER $a $t $f $o) $SUFIX)
"^\b$  cbt"
(joinStr "" $PREFIX (joinStr $SPLITTER $c $b $t) $SUFIX)
"^\b$  rtfm"
(joinStr "" $PREFIX (joinStr $SPLITTER $r $t $f $m) $SUFIX)
"^\b$  fyfi"
(joinStr "" $PREFIX (joinStr $SPLITTER $f $y $f $i) $SUFIX)
"^\b$  af"
(joinStr "" $PREFIX (joinStr $SPLITTER $a $f) $SUFIX)
"^\b$  wtf"
(joinStr "" $PREFIX (joinStr $SPLITTER $w $t $f) $SUFIX)
"^\b$  ass"
(joinStr "" $PREFIX (joinStr $SPLITTER $a $s $s) $SUFIX)
"^\b$  dumbass"
(joinStr "" $PREFIX (joinStr $SPLITTER $d $u $m $b $a $s $s) $SUFIX)
"^\b$  milf"
(joinStr "" $PREFIX (joinStr $SPLITTER $m $i $l $f) $SUFIX)
"^\b$  hell(a)"
(joinStr "" $PREFIX (joinStr "[^A-z0-9'`]*" $h $e $l $l (joinStr "" $a "?") ) $SUFIX)
"^\b$  mf/tf"
(joinStr "" $PREFIX (joinStr $SPLITTER_SPECIAL (joinStr "" "(" $m "|" $t ")") $f) $SUFIX)
"^\b$  asf"
(joinStr "" $PREFIX (joinStr $SPLITTER $s $s $f) $SUFIX)
)}}

{{/* when concatanating we add regex to make sure the word starts/ends with a space/new line character */}}
{{ $exactWordRegex := (joinStr "|"
"^\b$  free nitro site"	
".+nitro.+(http(s?))?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)"

"^\b$  dam[m/n](it/ed)"
(joinStr "" $PREFIX (joinStr $SPLITTER $d (joinStr "" "(" $a "|[*#])+") (joinStr "" $m "+") "[mMnN]*" (joinStr "" "(" (joinStr "" $i $t) "|" (joinStr "" $e $d) ")?") ) $SUFIX)
"^\b$  [d/z]am[m/n]"
(joinStr "" $PREFIX (joinStr $SPLITTER (joinStr "|" "(" $d $z ")") (joinStr "" $a "+") (joinStr "" $m "+") "[mMnN]+" ) $SUFIX)
"^\b$  [god]dammn"
(joinStr "" (joinStr $SPLITTER $d $a (joinStr "" $m "+") $n) $SUFIX)
"^\b$  d(r)am[m/n]"
(joinStr "" $PREFIX (joinStr $SPLITTER $d (joinStr "" $r "?") (joinStr "" $a "+") (joinStr "" $m "+") "[mMnN]+" ) $SUFIX)
"^\b$  dayum"
(joinStr "" $PREFIX (joinStr $SPLITTER $d $a $y $u $m) $SUFIX)
"^\b$  arse"
(joinStr "" $PREFIX (joinStr $SPLITTER $a $r $s $e) $SUFIX)
"^\b$  piss(ed)"
(joinStr "" $PREFIX (joinStr $SPLITTER $p $i $s $s (joinStr "" "(" $e $d ")?") ) $SUFIX)
"^\b$  orgasm"
(joinStr "" (joinStr $SPLITTER $o $r $g $a $s $m ))
"^\b$  bast(a/u)rd"
(joinStr "" (joinStr $SPLITTER $b $a $s $t (joinStr "|" "(" $a $u ")") $r $d ) $SUFIX)
"^\b$  bitch"
(joinStr "" $PREFIX (joinStr $SPLITTER $b $i $t $c $h ))
"^\b$  sh(i/e)t"
(joinStr "" $PREFIX (joinStr $SPLITTER $s $h (joinStr "|" "(" $i $e ")") $t) $SUFIX)
"^\b$ (bull/dog)shit(me)"
(joinStr "" (joinStr "" $s $h $i $t))
"^\b$  cock"
(joinStr "" $PREFIX (joinStr $SPLITTER $c $o (joinStr "" $c "?") $k) $SUFIX)
"^\b$  dick"
(joinStr "" (joinStr $SPLITTER $d $i $c $k))
"^\b$  fag(git/got)"
(joinStr "" $PREFIX (joinStr $SPLITTER $f $a $g (joinStr "" "(" (joinStr "" $g $i $t) "|" (joinStr "" $g $o $t) ")?") ) $SUFIX)
"^\b$  f(u)(c)k"
(joinStr "" $PREFIX (joinStr $SPLITTER $f (joinStr "" $u "*") (joinStr "" $c "*") $k))
"^\b$  _fuuuccck"
(joinStr "" (joinStr $SPLITTER $f (joinStr "" $u "+") (joinStr "" $c "+") $k))
"^\b$  fk"
(joinStr "" $PREFIX (joinStr $SPLITTER $f $k) $SUFIX)
"^\b$  jizz"
(joinStr "" $PREFIX (joinStr $SPLITTER $j $i $z $z) $SUFIX)
"^\b$  negro"
(joinStr "" (joinStr $SPLITTER $n $e $g $r $o))
"^\b$  nig(ga)(ger)(s)"
(joinStr "" $PREFIX (joinStr $SPLITTER $n (joinStr "" "(" $i "|" $o "|8)") $g (joinStr "" "(" (joinStr "" $g $a) "|" (joinStr "" $g $e $r) ")?") (joinStr "" "(" $s ")?") ) $SUFIX)
"^\b$  penis"
(joinStr "" $PREFIX (joinStr $SPLITTER $p $e $n $i $s))
"^\b$  retarted"
(joinStr "" $PREFIX (joinStr $SPLITTER $r $e $t $a $r $t $e $d) $SUFIX)
"^\b$  orgasm"
(joinStr "" $PREFIX (joinStr $SPLITTER $o $r $g $a $s $m) $SUFIX)
"^\b$  pussy"
(joinStr "" $PREFIX (joinStr $SPLITTER $p $u $s $s $y) $SUFIX)
"^\b$  whore"
(joinStr "" $PREFIX (joinStr $SPLITTER $w $h $o $r $e) $SUFIX)
"^\b$  slut"
(joinStr "" $PREFIX (joinStr $SPLITTER $s $l $u $t) $SUFIX)
"^\b$  cunt"
(joinStr "" $PREFIX (joinStr $SPLITTER $c $u $n $t) $SUFIX)
"^\b$  porn"
(joinStr "" $PREFIX (joinStr $SPLITTER $p $o $r $n))
"^\b$  hent(a)i"
(joinStr "" $PREFIX (joinStr $SPLITTER $h $e $n $t (joinStr "" $a "?") $i))
"^\b$  retard"
(joinStr "" $PREFIX (joinStr $SPLITTER $r $e $t $a $r $d))
)}}

{{ $regex := (joinStr "|" $acronymRegex $exactWordRegex ) }}

{{ $blockedMessageResponse := "Be nice, keep the chat PG. \n\n**You've been sent a DM** (if you have DM's enabled) **with the blocked message** so it can be edited. \n\nAttempting to circumvent this bot may cause the blocks to become more strict." }}
{{ $tempMessageDisplayTimeInSeconds := 25 }}



{{/* find all regex matches */}}
{{$regexMatches := reFindAll $regex .Message.Content}}


{{if ne (len $regexMatches) 0 }}
	{{/* This message contained at least one blocked word */}}
	
	
	
	{{/* underline the blocked words */}}
	{{$messageUnderlined := ""}}
	{{/* Loops through every substring between the regex matches */}}
	{{- range $regexMatchCount, $subString := (reSplit $regex .Message.Content)}}
		{{$messageUnderlined = (joinStr "" $messageUnderlined $subString)}}
		
		{{/* Skips adding the underlined regex match, after the final substring  */}}
		{{if (gt (len $regexMatches) $regexMatchCount)}}
			{{$messageUnderlined = (joinStr ""  $messageUnderlined "__" (index $regexMatches $regexMatchCount) "__")}}
		{{end}}
	{{- end}}
	
	
	{{/* send a direct message to the offender */}}
	{{$embed := cembed 
		"title" "Message Deleted" 
		"description" $blockedMessageResponse
		"color" 4645612 
		"fields" (cslice 
			(sdict "name" "Your Message" "value" $messageUnderlined "inline" false)
			(sdict "name" "Blocked Word(s)" "value" (joinStr ", " (reFindAll $regex .Message.Content)) "inline" false)
			(sdict "name" "Note" "value" "Attempting to circumvent this bot may cause the blocks to become more strict." "inline" false) 
		)
	}}
	{{ sendDM $embed }}
	
	
	
	
	{{ deleteMessage nil .Message.ID 0 }}
	
	
	{{/* send a message in the chat that the message was deleted */}}
	{{$embed := cembed 
		"description" (joinStr " " .User.Mention $blockedMessageResponse)
		"color" 4645612 
	}}
	{{ $tempMessageId := sendMessageRetID nil $embed }}
	{{ deleteMessage nil $tempMessageId $tempMessageDisplayTimeInSeconds }}
	
	
	{{/* Log the deleted message */}}
	{{$embed := cembed 
		"title" "Deleted Message" 
		"description" $messageUnderlined
		"color" 4645612 
		"fields" (cslice 
			(sdict "name" "Blocked Word(s)" "value" (joinStr ", " (reFindAll $regex .Message.Content)) "inline" false)
			(sdict "name" "User" "value" (.User.Mention) "inline" true) 
			(sdict "name" "Channel" "value" (.Channel.Mention) "inline" true) 
		)
		"thumbnail" (sdict "url" (joinStr "" "https://cdn.discordapp.com/avatars/" (toString .User.ID) "/" .User.Avatar ".png")) 
		"footer" (sdict 
			"text" "YAGPDB.xyz " 
			"icon_url" "https://cdn.discordapp.com/avatars/204255221017214977/2fa57b425415134d4f8b279174131ad6.png"
		)
		"timestamp" .Message.Timestamp
	}}
	{{ sendMessage "bot-log" $embed }}
	
{{ end }}
